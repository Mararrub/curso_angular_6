export class DestinoViaje {
	private selected:boolean;
	servicios: string[];

	constructor(public nombre: string, public u: string) {

		this.servicios = ['piscina', 'desayuno', 'almuerzo', 'cena', 'gimnasio', 'zona de masajes'];
	}	
	isSelected(): boolean {
		return this.selected;
	}

	setSelected(s:boolean) {
		this.selected = s;
	}
}
